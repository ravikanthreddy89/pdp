#include "mpi.h"
#include<stdio.h>
#include<string.h>
int main(argc, argv)
     int argc;
     char *argv[];
{
  int numtasks, rank, len,rc;
  int message;
 
  rc=MPI_Init(&argc,&argv);
  if(rc!=MPI_SUCCESS){
    printf("Error in initializing the MPI_Environment\n");
    MPI_Finalize();
  }

  MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  //  message_size=atoi(argv[1]);
  //  message=(char *)malloc(sizeof(char)*256);
 //if you are master process broadcast the message...
  if(rank==0){
    //sprintf(send_message,"Message"); 
    message=10;
       }
  MPI_Bcast(&message,1, MPI_INT,0,MPI_COMM_WORLD);
  MPI_Barrier(MPI_COMM_WORLD);
  //if you are not master process receive the message from master process and print to the stdout
  if(rank==0){
    printf("I am master and i sent message : %d\n",message);
  }else
 printf("I am process %d and message rxd : %d \n",rank,message);

 // free(message);
  MPI_Finalize();
  return 0;


}
