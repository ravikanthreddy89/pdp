#include "mpi.h"
#include <stdio.h>
#define MASTER 0

int main(argc,argv)
     int argc;
     char *argv[];
{
  int rc;
  int numtasks,rank, len;
  char hostname[MPI_MAX_PROCESSOR_NAME];
  int taskid;
  char message[100];
  int i, j;
  MPI_Status status;

  rc=MPI_Init(&argc,&argv);
  if(rc!=MPI_SUCCESS){
    printf("error in initializing the mpi environment");
    MPI_Finalize();  
  }

  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&numtasks);

  if(rank==0){
    for(i=1;i<numtasks;i++){
      MPI_Recv(message,256,MPI_CHAR,i,1,MPI_COMM_WORLD,&status);
      fputs(message,stdout);
    }
  }
  else {
    sprintf(message,"Message from process %d\n", rank);
    MPI_Send(message, strlen(message)+1, MPI_CHAR, 0,1,MPI_COMM_WORLD);
   }
  MPI_Finalize();
  return 0;
}
