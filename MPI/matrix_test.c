#include "mpi.h"
#include<stdio.h>
#define row 1
#define row_result 2 

int main(argc, argv)
     int argc;
     char *argv[];
{
  int ntasks, rank, count;
  int i,j; 
  MPI_Status status;
  int a[3][3];
  int b[3][3];
  int c[3][3];

  MPI_Init(&argc,&argv); 
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  if(rank==0){
    // initialize the a and b arrays
  for(i=0;i<3;i++){
    for(j=0;j<3;j++){
     b[i][j]=(i+j);
      a[i][j]=(i+j);
     }//end of inner for loop
   }

 }
  MPI_Bcast(b,9,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Barrier(MPI_COMM_WORLD);

  /*send the rows in matrix a to each process*/
  if(rank==0){
    for(i=0;i<3;i++){
      MPI_Send(a[i],3,MPI_INT,i+1,row,MPI_COMM_WORLD);
     }
    for(i=0;i<3;i++){
      MPI_Recv(c[i],3,MPI_INT,i+1,row_result,MPI_COMM_WORLD,&status);
     }
    printf("this is master process and the result matrix is \n");
    for(i=0;i<3;i++){
     for(j=0;j<3;j++){
       printf("%d  ",c[i][j]);
      }//end of inner for loop
     printf("\n");
    }
  }
  /*receive rows from the master process*/
  if(rank!=0) {
    MPI_Recv(a[0],3,MPI_INT,0,row,MPI_COMM_WORLD,&status);
    /*calculate  the row elements in ther result matrix  */
    for(i=0;i<3;i++){
      a[1][i]=0;
      for(j=0;j<3;j++){
        a[1][i]=a[1][i]+a[0][j]*b[j][i];
       }
    } 
    /*return the result back to master process*/
    MPI_Send(a[1],3,MPI_INT,0,row_result,MPI_COMM_WORLD);
  }
 
  MPI_Finalize();
  return 0; 
}// end of main function
