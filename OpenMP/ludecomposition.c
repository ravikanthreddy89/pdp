#include<stdio.h>
#include<stdlib.h>

int main(argc,argv)
     int argc;
     char *argv[];
{
  // int a[3][3],b[3][3];
  int i,j,k;

  double a[3][3]={ {25,5,1} , {64,8,1} , {144,12,1} };
  double b[3][3]={ {25,5,1} , {64,8,1} , {144,12,1} };
  double l[3][3]={{0}};
  double u[3][3]={{0}};

  for(i=0;i<3;i++){
    for(j=0;j<3;j++){
      printf("%lf ",a[i][j]);
      }
    printf("\n");
   }

  printf("--------------------------------------------------------\n");
  for(i=0;i<3;i++){
    for(j=0;j<3;j++){
      printf("%lf ",b[i][j]);
      }
    printf("\n");
   }

 printf("--------------------------------------------------------\n");

  for(k=0;k<3;k++){
    
    for(j=k;j<3;j++)
    u[k][j]=a[k][j];
 
    for(j=k+1;j<3;j++){
      a[k][j]=a[k][j]/a[k][k];//Scaling the pivot element
      //     u[k][j]=a[k][j];
    }

    l[k+1][k]=a[k+1][k]/a[k][k];

    for(i=k+1;i<3;i++){
      for(j=k+1;j<3;j++){
        a[i][j]=a[i][j]-a[i][k]*a[k][j];
       } 
    }
  }


  printf("---------------------Print L and U matricies------------------------\n ");

  
 for(i=0;i<3;i++){
    for(j=0;j<3;j++){
      printf("%lf ",l[i][j]);
      }
    printf("\n");
   }

 printf("--------------------------------------------------------\n");
 for(i=0;i<3;i++){
    for(j=0;j<3;j++){
      printf("%lf ",u[i][j]);
      }
    printf("\n");
   }
  printf("--------------------------------------------------------\n");



  return 0;
}
