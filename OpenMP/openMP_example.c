#include "omp.h"
#include<stdio.h>

void main(){

  omp_set_num_threads(4);
#pragma omp parallel
  {
    int id;
    id=omp_get_thread_num();
    printf("hello this is %d\n",id);
    printf("world this is %d\n",id); 
   } 
  return;
}
