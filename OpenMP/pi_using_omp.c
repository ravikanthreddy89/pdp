#include "omp.h"
#include<stdio.h>
#include<sys/time.h>

static long num_steps=100000;
double step;

void main(){
  int i;
  double pi,sum=0.0;
 
  double elapsed=0;
  //  double  start, end;

  double temp_sum[2];
  struct timeval tv; 

  double start_time;  
  double end_time;
  gettimeofday(&tv,0);
  start_time=(double)tv.tv_usec; 
  step=(1/((double)num_steps));

 double intervial = num_steps/2.0;
  printf("inetervial : %lf\n",intervial);
  omp_set_num_threads(2);
#pragma omp parallel 
  {
    int j;
    int id;
   double x=0.0;
   double start=0.0;
   double end=0.0; 
   id=omp_get_thread_num();
    temp_sum[id]=0.0;

    printf("thread's interviel is %lf \n",intervial);    
  start=(id*intervial);
  end=((id+1)*intervial);
 
  printf("ID: %d: start : %lf : end : %lf \n",id,start,end);
  for(j=start;j<end;j++){
    x=(j+0.5)*step;
    //sum=sum+(4.0/(1.0+(x*x)));
    temp_sum[id]=temp_sum[id]+(4.0/(1.0+(x*x)));
    }

}


 for(i=0;i<2;i++) sum=sum+temp_sum[i]; 


  pi=step*sum;
  gettimeofday(&tv,0);
  end_time=(double)tv.tv_usec; 
  
  elapsed=((double)(end_time-start_time));
  printf("Pi = %lf\n",pi);
  printf("elapsed time= %lf\n",elapsed);
  return ;
}
